var Staff = require("../models/staff");
var Credentials = require("../models/credentials");
var Patient = require("../models/patients");
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const _ = require("lodash");
// const JWT_SECRET = "qwqwjkdiuwejiikjkj";
const JWT_SECRET = process.env.JWT_SECRET;

// const { access } = require("node:fs");

module.exports ={

    // create: async (req, res) => {
    //     // console.log(req.body)
    //     let userData = req.body;
        
    //     try{

    //         let isUserExists = await Staff.findOne({ username: userData.username});

    //         // if(!isUserExists){
    //         //     return res.status.send("User already Exist");
    //         // }

    //         let tokenData = {
    //             username: userData.username,
    //         }

            
    //         let jwtToken = jwt.sign(tokenData, process.env.JWT_SECRET, { expiresIn: 86400});
    //         userData.verficationToken = jwtToken;

    //     console.log(jwtToken)


    //         let user = await Staff.create(userData);

    //         user.password ? delete user.password : null;
    //         if(!user){
    //             return res.status(200).send("OK");
    //         }
    //     }catch (err) {
    //         return res.status(500).send("not found")
    //     }
    // },

    register: async (req, res) => {
        let userData = req.body;
        // console.log(userData)
        try {
        //   let isVerificationRequired = userData.isVerificationRequired;
        // console.log(userData)
          
          let isUserExists = await Staff.findOne({ username: userData.username });
        //   console.log("user",isUserExists)
          if (isUserExists) {
            return res.status(400).json({status:'Not Found'});
          }
          let tokenData = {
            username: userData.username,
            password: userData.password
          }

          console.log(JWT_SECRET)
          let jwtToken = jwt.sign(tokenData, JWT_SECRET, { expiresIn: 86400 }); // expires in 24 hours
          userData.verificationToken = jwtToken;
          if(jwtToken){
              userData.token = JWT_SECRET;
          }

          console.log(userData.verificationToken)
    
          let user = await Staff.create(userData);
          
          user.password ? delete user.password : null;
    
          return res.status(200).json({
              status:"ok"});
        } catch (err) {
          // throwing error response to the client
          return res.status(500).send("Error");
        }
      },

    tokenVerfication: async (req, res, next) => {
        try
        {
            let token = req.param.id;
            if(!token){
                return res.status(401).send("No token Provide");
            }

            let decoded = await jwt.verify(token, process.env.JWT_SECRET)

            if(decoded){
                const user = await Staff.findOneAndUpdate(
                    {username: decoded.username},
                    { $set : {isUserVerified: true}}
                );

                const userObj = {
                    id: user._id
                }
                await Credentials.create(userObj);

                return res.status(200).send("verified");
            }else{
                return res.status(400).send("Error");
            }
        }catch (err) {
            return res.status.send("error")
        }
    },

    login: async (req, res) => {
        try{

            let authenticatedUser = await Staff.authenticate(req.body.username, req.body.password );
            console.log(authenticatedUser)
            return res.status(200).json({ authenticatedUser});
        }catch (err){
            if(err.message === "User_Not_Exit" || err.message === "Wrong_Password"){
                return res.status(400).send("Invalid username or password");
            } 
            return res.status(500).send("error");
        };
    },

    verify: async (req, res) => {
        try{
            var userData = req.body;
            var token = userData.token;
            if(!token) return res.status(401).send({auth: false, message: "No token provided"});
            let finalToken = token.split("Bearer");

            jwt.verify(finalToken[1], process.env.JWT_SECRET, function(err, decoded) {
                if (err) return res.status(500).send({ auth: false, message: "failed to authenticate token"});

                return res.status(200).send("Token is valid");
            })

        }catch (err) {
            return res.status(500).send("Error")
        }
    },

    create: async (req, res) => {
        try {
            // console.log("Hello")
            var userData = req.body;
            // console.log("user", userdata)

            var token = userData.token;
            console.log("token", token)
            const tok = await Staff.findOne({ token: token})
            console.log("tok", tok)

            // if(tok){
                const user = await Patient.create(req.body);
                res.status(200).send("created sucessfully");
            // }else {
            //     res.status(400).send("Invalid token")
            // }
          
        } catch(err){
          return res.status(400).send("Error");
        }
      },

      getData: async (req, res) => {
        try {
    
          let userData = req.body;
          let tok = userData.token;
          console.log(tok)
    
          const data = await Staff.findOne(
            // finding user with token
            { token: tok }
          );
           console.log(data)
          if (!data) {
            // throwing an error if user not found
            return res.status(404).send('data not found');
          } else {
            // throwing success response if found
            const user = await Patient.findOne({ vital_sign: userData.vital_sign, patient: userData.patient})
            res.status(200).send({user});
          }
        } catch (err) {
          // throwing any other errors if occured
          return res
            .status(400)
            .send(null);
        }
    },

    logout: async (req, res) => {
        try {
          // Find user and update sessionId with null
          const isUserLoggedOut = await Staff.findOneAndUpdate(
            { username: req.body.username, sessionId: req.body.sessionId },
            { $set: { sessionId: null } },
            { new: true }
          );
    
          if (isUserLoggedOut) {
            return res.status(200).send("User logged out successfully");
          } else {
            return res.status(404).send("Access denied");
          }
        } catch (err) {
          // throwing error response to the client
          return res.status(500).send("error");
        }
      },

};