const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const patientsSchema = new mongoose.Schema({
    patient: {
        type : String,
        required: true,
    },
    vital_sign:
    {
        type: String,
        required: true,
    },
    value: {
        type : Number,
        required: true,

    },
    note: {
        type : String,
        required: true,

    },
    token: {
        type: String,
        require: true
    }
},
{
    timestamps: true
  });

module.exports = mongoose.model("Patient", patientsSchema)