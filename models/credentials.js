const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const credentialSchema = new mongoose.Schema({
    id: {
        type : String,
        required: true,
    },
    password:
    {
        type: String,
        required: true,
        min: 6
    }
},
{
    timestamps: true
  });

module.exports = mongoose.model("Credentials", credentialSchema)