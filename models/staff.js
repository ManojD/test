const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const staffSchema = new mongoose.Schema({
    // id: {
    //     type : String,
    //     required: true,
    // },
    username:
    {
        type: String,
        required: true,
    },
    fullname:{
        type: String,
        // required: true,

    },
    isUserVerified:{
        type: Boolean
    },
    verificationToken: {
        type: String
    },
    token: {
      type: String
    },
    password:
    {
        type: String,
        required: true,
        min: 6
    },
    sessionId: {
        type: String
    }
},
{
  timestamps: true
}
);

staffSchema.pre("save", async function(next) {
    const user = this;
    const hashedPassword = await bcrypt.hash(user.password, 10);
    user.password = hashedPassword;
    next();
  })
  
  staffSchema.statics.authenticate = async (username, password) => {
    let getUser = await Staff.findOne(
      { username: username },
      { _id: 1, username: 1, password: 1, fullname: 1 }
    );
  
    if( !getUser ){
      throw new Error("User_Not_Exist");
    }
  
    const isValidPassword = await bcrypt.compare(password, getUser.password);
    if (isValidPassword !== true) {
      throw new Error("Wrong_Password")
    }
  
    const sessionId = await crypto.randomBytes(20).toString("hex");
    let userData = await Staff.findOneAndUpdate(
      { username: username },
      { $set: { sessionId } },
      { new: true }
    );
  
    let tokenData = {
        username: userData.username,
      fullname: userData.fullname,
      _id: userData._id
    }
  
    let jwtToken = jwt.sign( tokenData, process.env.JWT_SECRET, { expiresIn: 86400 }); // expires in 24 hours
  
    userData.password = undefined;
    userData.token = jwtToken;
    return { data: userData, token: userData.token };
  };
  

var Staff = mongoose.model("Staff", staffSchema);

module.exports = Staff;