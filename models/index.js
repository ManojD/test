const dbConfig = require("../config/db.config");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.staff = require("./staff")(mongoose);
db.patients = require("./patients")(mongoose);
db.credentials = require("./credentials")(mongoose);

module.export = db;