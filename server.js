const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const cors = require("cors");
const db = require("./models")
const staff = require("./controllers/auth")

const app = express();

var corsOption = {
    origin: process.env.baseURL
};

app.use(cors(corsOption));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extented:true
}));

app.get("/pong", (req, res)=>{
    res.send("Ping")
});

app.post("/api/user/register", staff.register);

app.post("/api/user/login", staff.login);

app.post("/api/user/create", staff.create);

app.post("/api/user/logout", staff.logout);

app.get("/api/user/confirmation/:id", staff.tokenVerfication);

app.get("/api/user/verify/access-token", staff.verify);

app.get("/api/user/verify/getData", staff.getData);

dotenv.config();

// db.mongoose
mongoose.connect(process.env.DB_CONNECT, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log("connected to the db");
    })
    .catch(err => {
        console.log("cannot connect to db", err);
        process.exit();
    });

app.use(express.json(), cors());


const PORT = process.env.PORT || 8080;

require("./routes/auth/staff")(app);

app.listen(PORT, ()=>{
    console.log(`server connected at: ${PORT}`);
})