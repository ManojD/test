
module.exports = app =>{
    const staff = require("../../controllers/auth");

    var router = require("express").Router();

    // router.post("/register", staff.register);

    router.post("/login", staff.login);

    router.post("/logout", staff.logout);

    router.get("/confirmation/:id", staff.tokenVerfication);

    router.get("/verify/access-token", staff.verify);

    router.get("/verify/getData", staff.getData);

    app.use("/api/user", router)
};